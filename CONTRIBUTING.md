You all have your assigned jobs, but it's important to manage the repository such that it isn't a complete mess.
 Here's a brief refresher on how Git works from the user perspective. It isn't technical and should be easy to follow.
 https://www.atlassian.com/git/tutorials/comparing-workflows/centralized-workflow
 
 It isn't necessary to read the entire article. Just skim through the first section on Centralized Workflow. It assumes that the reader comes from a background in SVN, but those bits aren't too important. While the centralized flow should work, I recommend we attempt to use the Feature Branch Workflow, which is described directly after the Centralized Workflow. It isn't that much more complicated and it would allow us to work more efficiently.
 