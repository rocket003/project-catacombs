﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

	public bool isActive = false;
	public GameObject gameManager;
	public GameObject spawnPoint;
	public GameObject flame;


	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager");
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager.GetComponent<GameManager> ().getSpawnPoint () == spawnPoint) {
			isActive = true;
		} else {
			isActive = false;
		}
			
		flame.SetActive (isActive);
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			gameManager.GetComponent<GameManager> ().setSpawnPoint (spawnPoint);
		
		}


	}
}
