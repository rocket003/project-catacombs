﻿/*Programmer(s): Matt Wong
 * This script controls all basic movements of the player
 * Note: For the jumping mechanism to work, add an empty object under the parent of this object and place it on the very bottom-middle point of the object. 
    Drag the new child object to "Ground Point" in this script component. After that, set "Ground Mask" to the same layer as the ground object. 
    The child object is suppose to act as a sensor to detect if the object is on the ground.
 */
using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	//set jump speed, move speed and dashforce here
	public float moveSpeed = 5;
	public float jumpHeight = 300;
	public float dashForce = 1500;

	//cost of actions
	public int dashStamCost = 10;

	//detects ground
	public Transform groundPoint;
	public float radius;
	public LayerMask groundMask;
	bool isGrounded;

	bool facing = true;
	//helps revert to original speed
	float baseSpeed;

	Rigidbody2D rb2D;

	//references PlayerData script
	GameObject player;
	PlayerData playerScript;

	//animation component initialization (Nic Added this)
	Animator anim;
	public int speed;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D>();
		baseSpeed = moveSpeed;
		player = GameObject.Find ("PlayerData");
		playerScript = gameObject.GetComponent<PlayerData> ();
	
		//finds the Animator component (Nic Added this)
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

		//moving
		Vector2 moveDir = new Vector2 (Input.GetAxisRaw ("Horizontal") * moveSpeed, rb2D.velocity.y);
		rb2D.velocity = moveDir;

		//checking if player is on the ground
		isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask);
		anim.SetBool ("grounded", isGrounded);


		//switching faces
		if (Input.GetAxisRaw("Horizontal") == -1 && facing == true)
		{

			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			facing = false;

		}
		else if (Input.GetAxisRaw("Horizontal") == 1 && facing == false)
		{

			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
			facing = true;
		}

		//start jump
		if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
		{
			rb2D.AddForce(new Vector2(0, jumpHeight));

		}

		//dash
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			int newStam = playerScript.stamina - dashStamCost;
			if (newStam > 0 && rb2D.velocity.x != 0 && rb2D.velocity.y != 0) {
				rb2D.AddForce (new Vector2 (rb2D.velocity.x * dashForce, 0));
				playerScript.descreaseStamina(dashStamCost);
			}
		}

		//set animation variables (Nic Added this)
		speed = Mathf.Abs((int)Input.GetAxisRaw ("Horizontal"));
		anim.SetInteger ("Speed", speed);

	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(groundPoint.position, radius);
	}
}
