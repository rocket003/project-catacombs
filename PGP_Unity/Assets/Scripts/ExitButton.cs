﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExitButton : MonoBehaviour {

	public Button exitButton;

	void start(){
		exitButton = exitButton.GetComponent<Button> ();
	}

	public void loadMenu(){
		Destroy(GameObject.FindGameObjectWithTag("Player"));
		Destroy(GameObject.FindGameObjectWithTag("HUD"));
		SceneManager.LoadScene (0);
	}
}
