﻿/*Programmer: Matt Wong
 * Desc: This class acts as the superclass for all weapons used by enemies and players
 * This class just holds generic weapon
 */
using UnityEngine;
using System.Collections;
using System;

public class Weapons : MonoBehaviour {
	
	public int baseDamage = 3; //the damage that the weapon can do
    public float attkDist = .3f; //distance where weapon reach
    public float addCoolDown = .3f; //cool down
    public int staminaCost = 3;

	// Use this for initialization
	void Start () {
	}
   
    //returns the base damage of the weapon
	public int getDamage(){
		return baseDamage;
	}
    
    //returns the attack distance of the weapon
    public float getAttackDistance()
    {
        return attkDist;
    }

    //returns the attack rate per second
    public float getAttkCoolDown()
    {
        return addCoolDown;
    }
    
    public int getStaminaCost()
    {
        return staminaCost;
    }
		
}
