﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public string levelToLoad;

	// Objects to save
	public GameObject player;
	public Canvas HUD;
	public GameObject[] weapons;
	public GameObject cam;

	public Texture2D fadeTexture;
	public float fadeSpeed = 0.0f;  
	public LayerMask everythingMask;

	private int drawDepth = -1000;  //texture's order in the hierarchy
	private float alpha = 1.0f;     // 
	private int fadeDirection = -1; //direction to fade.  fade in = -1, fade out = 1



		// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		HUD = Canvas.FindObjectOfType<Canvas> ();

	}

	void OnGUI(){
		alpha += fadeDirection * fadeSpeed * Time.deltaTime;
		alpha = Mathf.Clamp01 (alpha);

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);         // set the color of the new texture
		GUI.depth = drawDepth;                                                        // set the depth of the new texture
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeTexture);  // create new texture
	}	

	void Update(){
		//weapons = player.GetComponent<PlayerData>().getWeapons ();

	}

	IEnumerator OnTriggerEnter2D (Collider2D col) {

		if (col.tag == "Player") {
			player = GameObject.FindGameObjectWithTag ("Player");
			HUD = Canvas.FindObjectOfType<Canvas> ();

			float fadeTime = BeginFade (1);

			yield return new WaitForSeconds (fadeTime);

			cam = GameObject.FindGameObjectWithTag("MainCamera"); //Disable all the camera can see while old scene loads out
			cam.GetComponent<Camera> ().cullingMask = 0;

			weapons = player.GetComponent<PlayerData>().getWeapons ();
			foreach (GameObject weapon in weapons) {
				if(weapon != null)
					DontDestroyOnLoad(weapon);
			}

			DontDestroyOnLoad (cam);
			DontDestroyOnLoad (HUD);
			DontDestroyOnLoad (player);
			player.transform.position = new Vector3 (0, 0, 0);
			SceneManager.LoadScene (levelToLoad, LoadSceneMode.Single);
		}
	}

	public float BeginFade(int direction){

		fadeDirection = direction;
		return(fadeSpeed);
	}

	void OnLevelWasLoaded(){
		player = GameObject.FindGameObjectWithTag ("Player");
		cam = GameObject.FindGameObjectWithTag("MainCamera"); 
		cam.GetComponent<Camera> ().cullingMask = everythingMask; //Restore all vision to the camera
		BeginFade (-1);
	}

}
