﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public Camera cam;

	public float leftBoundary;
	public float rightBoundary;
	public float bottomBoundary;
	public float topBoundary;

	public float cameraWidth;
	public float cameraHeight;

	public float midX; //Mid point of the extreme player locations (determines where camera is centered)
	public float midY;
	public float cameraDistance;

	// Use this for initialization
	void Start () {
		/*
		cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		cameraHeight = 2f * cam.orthographicSize; // size gives from the mid point to the top of the camera
		cameraWidth = cameraHeight * cam.aspect;
		*/
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		//Next Draw the bounds of the map
		Gizmos.DrawWireCube (new Vector2 ((rightBoundary + leftBoundary) / 2, (topBoundary + bottomBoundary) / 2), new Vector2 (rightBoundary - leftBoundary, topBoundary - bottomBoundary));
	}

	// Update is called once per frame
	void Update () {
		if (GameObject.FindGameObjectWithTag ("Player")) {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			midX = player.transform.position.x;
			midY = player.transform.position.y;
		}

		if (midX > rightBoundary - cameraWidth / 2)
			midX = rightBoundary - cameraWidth / 2;
		if (midX < leftBoundary + cameraWidth / 2)
			midX = leftBoundary + cameraWidth / 2;
		if (midY > topBoundary - cameraHeight / 2)
			midY = topBoundary - cameraHeight / 2;
		if (midY < bottomBoundary + cameraHeight / 2)
			midY = bottomBoundary + cameraHeight / 2;

		cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		//cameraHeight = 2f * cam.orthographicSize; // size gives from the mid point to the top of the camera
		cameraHeight = 2.0f * -1 * cameraDistance * Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
		cameraWidth = cameraHeight * cam.aspect;
		cam.transform.position = new Vector3 (midX, midY, cameraDistance);
	}
}
