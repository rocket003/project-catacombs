﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTest : MonoBehaviour {

	private Collider2D co2D;

	void Start () {
		co2D = GetComponent<Collider2D> ();
	}

	// Update is called once per frame
	void Update () {
		Bounds bounds = co2D.bounds;

		Vector2 origin = new Vector2(transform.InverseTransformPoint(transform.right).x, bounds.center.y);
		Vector2 dir = new Vector2(transform.InverseTransformPoint(transform.right).x, Vector2.down.y);

		Debug.DrawRay(origin, dir * bounds.extents.y * 1.4f);

	}
}
