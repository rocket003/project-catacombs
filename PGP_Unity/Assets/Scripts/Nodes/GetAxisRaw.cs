﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

[NodeInfo(category = "Action/Input/", icon = "Axis")]
public class GetAxisRaw : ActionNode {

	[VariableInfo(requiredField = false, nullLabel = "Horizontal")]
	public StringVar axisName;

	[VariableInfo(canBeConstant = false)]
	public FloatVar storeAxis;
	[VariableInfo(canBeConstant = true)]
	public FloatVar multiplier;

	public override void Awake () {
		base.Awake ();

		if (axisName.isNone)
			axisName.Value = "Horizontal";
	}

	public override Status Update () {
		storeAxis.Value = Input.GetAxisRaw (axisName) * multiplier.Value;

		return Status.Running;
	}
}
