﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

[NodeInfo(category = "Action/Rigidbody2D/", icon = "Rigidbody2D")]
public class MovePosition : ActionNode {

	[VariableInfo(requiredField = false, nullLabel = "Use Self")]
	public GameObjectVar gameObject;


	// No Vector2Var, so use Vector3Var and throw out the Z-Component
	[VariableInfo(requiredField = false, nullLabel = "Don't Use", tooltip = "Direction to move the rigidbody")]
	public Vector3Var direction;
	[VariableInfo(requiredField = false, nullLabel = "Don't Use", tooltip = "Direction in the x axis (overrides direction x)")]
	public FloatVar x;
	[VariableInfo(requiredField = false, nullLabel = "Don't Use", tooltip = "Direction in the y axis (overrides direction y)")]
	public FloatVar y;

	[VariableInfo(tooltip = "Not Yet Implemented")]
	public Space relativeTo;

	[VariableInfo(tooltip = "If yes, object will move per second. If no, object will move instantly.")]
	public bool perSecond = true;

	[VariableInfo(tooltip = "If yes, object will fall with gravity")]
	public bool useGravity = false;

	private Vector2 moveDir;
	private Rigidbody2D rb;

	public override void Awake () {
		base.Awake ();

		if (gameObject.isNone)
			gameObject.Value = self;
			
		rb = gameObject.Value.GetComponent<Rigidbody2D> ();

		if (direction.isNone)
			direction.Value = Vector3.zero;
	}

	public override Status Update () {

		moveDir = new Vector2(x.isNone ? direction.vector2Value.x : x.Value, y.isNone ? direction.vector2Value.y : y.Value);

		if (useGravity && y.Value == 0)
			moveDir += Physics2D.gravity * rb.gravityScale;


		if (perSecond)
			moveDir *= Time.deltaTime;

		rb.MovePosition (rb.position + moveDir);

		Debug.Log (rb.velocity.ToString ());
		return Status.Running;
	}
}
