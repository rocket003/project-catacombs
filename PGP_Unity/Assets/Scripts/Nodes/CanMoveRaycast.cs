﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;


[NodeInfo(category = "Condition/Rigidbody2D/", icon = "Rigidbody2D")]
public class CanMoveRaycast : ActionNode {

	[VariableInfo(requiredField = false, nullLabel = "Use Self")]
	public GameObjectVar gameObject;

	[VariableInfo(requiredField = false)]
	public LayerMask groundMask;

	[VariableInfo(requiredField = true)]
	public FloatVar maxAngle;

	[VariableInfo(requiredField = false, tooltip = "The offset of the raycast, tweak as needed")]
	public FloatVar offset;

	private Collider2D co2D;

	public override void Awake () {
		if (gameObject.isNone)
			gameObject.Value = self;

		if (offset.isNone)
			offset.Value = 0;
		
		co2D = gameObject.Value.GetComponent<Collider2D> ();
	}

	public override Status Update () {
		Bounds bounds = co2D.bounds;

		float sign = Mathf.Sign (gameObject.Value.transform.localScale.x);

		Vector2 origin = new Vector2(bounds.center.x + (bounds.extents.x * sign), bounds.center.y);
		Vector2 dir = Vector2.down + (Vector2.right * sign * 0.5f);

		RaycastHit2D hit = Physics2D.Raycast (origin, dir, bounds.extents.y * 1.5f, groundMask);

		Debug.DrawRay(origin, dir.normalized * bounds.extents.y * 1.5f);

		if (hit && Vector2.Angle (hit.normal, Vector2.up) < maxAngle.Value)
			return Status.Success;
		
		return Status.Failure;
	}

}
