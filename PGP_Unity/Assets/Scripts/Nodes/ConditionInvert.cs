﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

[NodeInfo(category = "Decorator/")]
public class ConditionInvert : DecoratorNode {

	public override Status Update () {
		if (child == null)
			return Status.Error;

		child.OnTick ();

		if (child.status == Status.Failure)
			return Status.Success;

		return Status.Failure;
	}
}
