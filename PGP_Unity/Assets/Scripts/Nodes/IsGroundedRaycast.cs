﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

[NodeInfo(category = "Condition/Physics2D/", icon = "BoxCollider2D")]
public class IsGroundedRaycast : ConditionNode {

	[VariableInfo(requiredField = false, nullLabel = "Use Self")]
	public GameObjectVar gameObject;

	[VariableInfo(requiredField = false)]
	public LayerMask groundMask;

	[VariableInfo(requiredField = true)]
	public FloatVar skinWidth;

	[VariableInfo(requiredField = true)]
	public FloatVar rayLength;

	private Collider2D co2D;

	public override void Awake () {
		if (gameObject.isNone)
			gameObject.Value = self;
		
		co2D = gameObject.Value.GetComponent<Collider2D> ();
		rayLength.Value += skinWidth.Value;
	}
		
	public override Status Update () {
		Bounds bounds = co2D.bounds;
		bounds.Expand (skinWidth * -2);

		Vector2 rayOrigin = new Vector2 (bounds.center.x, bounds.min.y);

		RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.down, rayLength, groundMask);
		Debug.DrawRay (rayOrigin, Vector2.down * rayLength, Color.red);
		if (hit) {
			return Status.Success;
		}

		return Status.Failure;
	}
}
