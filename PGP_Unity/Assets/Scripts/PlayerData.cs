﻿/*Programmer(s): Matt Wong
 * This script controls the players health, stamina and weapons 
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{

	//declares the variable that keeps track of current health
	public int health = 100;
	public int stamina = 100;

	//declares variables that keep track of the maximum health
	public int maxHealth = 100;
	public int maxStam = 100;

	//declares text that will be displayed on the screen
	public Text healthDisplay;
	public Text staminaDisplay;

	//declares the damage that the player will inflict, the range of attack and how many times the player can attack
	public int baseDamage = 2;
	public float baseAttkDist = .7f;
	public float attkCoolDown = .1f;
	public int baseStaminaTake = 2;

	//declares array of weapons that the player is currently holding
	private GameObject[] weapons; //holds an array of prefab weapons
	private const int NUM_OF_WEPS = 4; //how many weapons the player can hold
	private int curWep;
	public Transform weaponHoldPoint;
	private const float weaponPickUpRangeRadius = 1.5f;

	//declares the an object for the Move script
	private Move moveScript;

	//declares regeneration rates
	private int healthIncreaseRate = 1;
	private int staminaIncreaseRate = 1;
	private float healthRegenTime = 0.5f;
	private float staminaRegenTime = 0.5f;

	// Use this for initialization
	void Start()
	{

		DisplayStats();
		health = maxHealth;
		stamina = maxStam;
		moveScript = gameObject.GetComponent<Move>();

		//iterating health and stamina increase here
		InvokeRepeating("healthBaseRegen", 1.0f, healthRegenTime);
		InvokeRepeating("staminaBaseRegen", 1.0f, staminaRegenTime);

		//initializes spaces for weapons and curWep 
		weapons = new GameObject[NUM_OF_WEPS];
		curWep = 0;

	}

	// Update is called once per frame
	void Update()
	{
		DisplayStats();
		if (health <= 0)
		{
			Destroy (this.gameObject);
			//death sequence
			Debug.Log("You died");
		}

		//picking up weapon which is activated by pressing 'G'. 
		if (Input.GetKeyDown(KeyCode.G))
		{
			GameObject[] weaponsPicked = colliderTagSorter("Weapon", getAllAround(weaponPickUpRangeRadius));

			//deactivates the number of weapons that are left empty in the inventory slots
			if (weaponsPicked.Length > 0)
			{
				for (int i = 0; i < weaponSpacesLeft(); i++)
				{
					if (weaponsPicked.Length > i)
					{
						activateObject(weaponsPicked[i].gameObject, false);
						//throws it far away so it wont be detected by player until called
						weaponsPicked[i].gameObject.transform.position = new Vector3(transform.position.x * 100f, transform.position.y * 100f, transform.position.z);
					}
				}
				//picks up weapons if there are inventory spaces left, if not, then replace weapon on current spot
				if (weaponsPicked.Length > 0 && weaponSpacesLeft() != 0)
				{
					for (int i = 0; i < weaponsPicked.Length; i++)
					{
						addWeaponToInven(weaponsPicked[i]);
					}
				}
				else if (weaponSpacesLeft() == 0)
				{
					//replaces the current weapon the player is holding if inventory is full. Closest weapon to player priority
					//adds weapon to current spot and drops weapon from current spot
					dropCurWep();
					activateObject(weaponsPicked[0], false);
					weaponsPicked[0].GetComponent<SpriteRenderer>().enabled = true;
					weapons[curWep] = weaponsPicked[0];
				}
			}
		}

		//drops the current weapon the player is holding
		if (Input.GetKeyDown(KeyCode.Q))
		{
			dropCurWep();
		}

		//weapon switching with numeric keys
		int tempPosWep = curWep;
		if (Input.GetKeyDown(KeyCode.Alpha1))
			curWep = 0;
		else if (Input.GetKeyDown(KeyCode.Alpha2))
			curWep = 1;
		else if (Input.GetKeyDown(KeyCode.Alpha3))
			curWep = 2;
		else if (Input.GetKeyDown(KeyCode.Alpha4))
			curWep = 3;
		if (curWep != tempPosWep)
			activateObject(weapons[tempPosWep], false);
		//attaches weapon to player
		if (weapons[curWep] != null)
		{
			GameObject wep = weapons[curWep];
			wep.GetComponent<SpriteRenderer>().enabled = true;
			Weapons curWepScript = wep.GetComponent<Weapons>();
			wep.transform.position = weaponHoldPoint.position;
			//locks rotation and set to the angle of the arm
			Transform arm = null;
			foreach (Transform child in transform)
			{
				if (child.name.ToLower().IndexOf("arm") > -1)
				{
					arm = child;
					break;
				}
			}

			Transform holdPoint = arm.GetChild(0).GetChild(0);
			wep.transform.rotation = Quaternion.Euler(0, 0, holdPoint.rotation.eulerAngles.z);

			if (transform.localScale.x < 0 && wep.transform.localScale.x > 0 || transform.localScale.x > 0 && wep.transform.localScale.x < 0)
				wep.transform.localScale = new Vector3(-wep.transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Weapon")
		{
			Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
		}
	}
	//displays the health and stamina on the screen
	void DisplayStats()
	{
		try{
			//finds the canvas object to implement the health and stamina bar
			GameObject canvas = GameObject.Find("Canvas(Clone)");
			healthDisplay = canvas.transform.GetChild(0).gameObject.GetComponent<Text>();
			staminaDisplay = canvas.transform.GetChild(1).gameObject.GetComponent<Text>();
			healthDisplay.text = "Health: " + health;
			staminaDisplay.text = "Stamina: " + stamina;
		}
		catch (System.Exception e)
		{
			Debug.Log("SET UP HEALTH AND STAMINA BAR YOU PLEB");
			Debug.Log("Health: " + health);
			Debug.Log("Stamina " + stamina);
		}
	}

	//regenerates health by the number indicated
	void healthBaseRegen()
	{
		if (health < maxHealth)
		{
			health = health + healthIncreaseRate;
		}
	}
	//regenerates stamina by the number indicated
	void staminaBaseRegen()
	{
		if (stamina < maxStam)
		{
			stamina = stamina + staminaIncreaseRate;
		}
	}
	//decreases stamina by set amount
	public void descreaseStamina(int toDecrease)
	{
		stamina = stamina - toDecrease;
	}
	//decreases health by set amount
	public void descreaseHealth(int toDecrease)
	{
		health = health - toDecrease;
		//knockback when taken damage - NOT TESTED
		GetComponent<Rigidbody2D>().AddForce(new Vector2(1.5f * transform.localScale.x,0));
	}

	//gets the base damage of the player
	public int getBaseDamage()
	{
		return baseDamage;
	}

	//gets the player base attack distance
	public float getBaseAttkDist()
	{
		return baseAttkDist;
	}

	public float getAttkCoolDown()
	{
		return attkCoolDown;
	}

	public int getBaseStaminaTake()
	{
		return baseStaminaTake;
	}

	//gets EVERYTHING around the player of a certain radius
	private Collider2D[] getAllAround(float radius)
	{
		return Physics2D.OverlapCircleAll(transform.position, radius);
	}

	//takes in an array of Collider2D objects and returns an array of GameObjects that have a certain tag
	private GameObject[] colliderTagSorter(string tagName, Collider2D[] toSort)
	{
		//for ease, use ArrayList to add items to array. Then convert back to array to send back. (MAY CHANGE LATER FOR MEMORY EFFICIENCY)
		ArrayList tempList = new ArrayList();
		for (int i = toSort.Length - 1; i >= 0; i--)
		{
			if (toSort[i].gameObject.tag == tagName)
			{
				tempList.Add(toSort[i].gameObject);
			}
		}
		return tempList.ToArray(typeof(GameObject)) as GameObject[];
	}

	//checks if inventory is full and if it isn't add weapon to empty slot. Returns true if added, returns false if inventory is full
	private bool addWeaponToInven(GameObject toAdd)
	{
		int emptySlot = checkIfWeaponsIsFull();
		if (emptySlot > -1)
		{
			weapons[emptySlot] = toAdd;
			return true;
		}
		return false;
	}

	//checks if the weapons array is full with GameObjects already. If it is, return -1, if not, return slot which is empty
	private int checkIfWeaponsIsFull()
	{
		for (int i = 0; i < weapons.Length; i++)
			if (weapons[i] == null)
				return i;
		return -1;
	}

	//counts how many empty spaces are left in the weapon inventory
	private int weaponSpacesLeft()
	{
		int cnt = 0;
		if (weapons != null)
		{
			for (int i = 0; i < weapons.Length; i++)
			{
				if (weapons[i] == null)
					cnt++;
			}
			return cnt;
		}
		return NUM_OF_WEPS;
	}

	public Weapons getCurWeapon()
	{
		if (weaponSpacesLeft() != NUM_OF_WEPS)
		if(weapons[curWep] != null)
			return weapons[curWep].GetComponent<Weapons>();
		return null;
	}
	//this function disables or enables all colliders, the sprite renderer and the rigidbody2D gravity scale of an object
	private void activateObject(GameObject obj, bool activate)
	{
		if (obj != null)
		{
			obj.GetComponent<SpriteRenderer>().enabled = activate;
			foreach (Collider2D c in obj.GetComponents<Collider2D>())
				c.enabled = activate;
			if (activate)
				obj.GetComponent<Rigidbody2D>().gravityScale = 1;
			else
				obj.GetComponent<Rigidbody2D>().gravityScale = 0;
		}
	}

	//drops the current weapon the player is holding
	private void dropCurWep()
	{
		if (weapons[curWep] != null)
		{
			weapons[curWep].transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			activateObject(weapons[curWep], true);
			weapons[curWep] = null;
		}
	}
	//draws overlap circle
	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, weaponPickUpRangeRadius);
	}

	public GameObject[] getWeapons(){
		return weapons;
	}
}

