﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

	public Canvas quitMenu;//add the canvas that holds the quit menu
	public Canvas loadMenu;//add the canvas that holds the load menu
	public Button startText;//add the play button from the start menu
	public Button loadText;//add the load button from the start menu
	public Button quitText;//add the quit button from the start menu

	// Use this for initialization
	void Start () {
		quitMenu = quitMenu.GetComponent<Canvas> ();
		loadMenu = loadMenu.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> ();
		loadText = loadText.GetComponent<Button> ();
		quitText = quitText.GetComponent<Button> ();
		quitMenu.enabled = false;//sets the canvas to be invisible at start
		loadMenu.enabled = false;//sets the canvas to be invisible at start
	}

	//brings up the quit menu
	public void exitPress(){
		quitMenu.enabled = true;//makes the canvas for quit appear
		loadMenu.enabled = false;
		startText.GetComponent<Text>().enabled = false;
		quitText.GetComponent<Text>().enabled = false;
		loadText.GetComponent<Text>().enabled = false;
	}

	//used to exit the game
	public void yesExit(){
		Application.Quit ();
		//System.Diagnostics.Process.GetCurrentProcess ().Kill ();
	}

	//The function for when the user presses load
	public void loadPress(){
		quitMenu.enabled = false;
		loadMenu.enabled = true;//makes the load canvas appear
		startText.GetComponent<Text>().enabled = false;
		quitText.GetComponent<Text>().enabled = false;
		loadText.GetComponent<Text>().enabled = false;
	}

	//function for loading a level
	//takes in a scene based on where it is in the build
	public void loadLevel(int level){
		SceneManager.LoadScene (level);
	}

	//function for when the user presses back
	public void backPress(){
		quitMenu.enabled = false;
		loadMenu.enabled = false;
		startText.GetComponent<Text>().enabled = true;
		quitText.GetComponent<Text>().enabled = true;
		loadText.GetComponent<Text>().enabled = true;
		Debug.Log ("called");
	}

	//starts a new game
	//loads in the first scene of the build (level 1- scene 1)
	public void startGame(){
		SceneManager.LoadScene(1);
	}

}