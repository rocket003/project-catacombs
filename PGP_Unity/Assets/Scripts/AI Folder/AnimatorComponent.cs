﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The AnimatorComponent encapsulates the necessary components for animation
// Do not inherit from AnimatorComponent, include an AnimatorComponent in the script defining the specific Actor
// Not to be confused with Unity's Animator component or Animator Controller
public class AnimatorComponent : MonoBehaviour {

	Animator anim;

	private bool facingRight = true;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	// Flip will flip the direction of the object, including the animator, and change the facingRight to its opposite value
	void Flip () {
		facingRight = !facingRight;
		transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}
