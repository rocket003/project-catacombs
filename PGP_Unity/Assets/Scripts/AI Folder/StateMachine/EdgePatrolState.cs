﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgePatrolState : IState {

	Actor actor;
	Vector2 moveDirection = Vector2.right;

	public void Enter (Actor actor)
	{
		this.actor = actor;
	
	}

	public void Exit ()
	{

	}

	public void StateUpdate ()
	{

	}

	public void StateFixedUpdate ()
	{

		Bounds bounds = actor.Co2D.bounds;
		bounds.Expand (Actor.skinWidth * -2);

		Debug.DrawRay ((Vector2)(bounds.min) , (Vector2.down + Vector2.left) * Actor.rayLength, Color.red);
		Debug.DrawRay (new Vector2(bounds.max.x, bounds.min.y) , (Vector2.down + Vector2.right) * Actor.rayLength, Color.red);

		if (Physics2D.Raycast (new Vector2(bounds.max.x, bounds.min.y), (Vector2.down + Vector2.right), Actor.rayLength,  actor.GroundMask).collider == null) {
			moveDirection = Vector2.left;
		}

		else if (Physics2D.Raycast ((Vector2)(bounds.min), (Vector2.down + Vector2.left), Actor.rayLength, actor.GroundMask).collider == null) {
			moveDirection = Vector2.right;
		}

		actor.Move (moveDirection);

	}

	public void OnTriggerEnter2D (Collider2D other)
	{

	}

	public void OnTriggerExit2D (Collider2D other)
	{

	}
		
}
