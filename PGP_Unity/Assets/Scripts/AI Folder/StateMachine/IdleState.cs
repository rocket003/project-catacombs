﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState {

	private Actor actor;

	public void Enter (Actor actor) {
		this.actor = actor;

	}

	public void Exit () {

	}

	public void StateUpdate () {
	
	}

	public void StateFixedUpdate () {

	}

	public void OnTriggerEnter2D (Collider2D other) {

	}

	public void OnTriggerExit2D (Collider2D other) {

	}

}