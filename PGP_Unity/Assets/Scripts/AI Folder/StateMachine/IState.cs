﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The IState is an interface used to create States for use in the State Machine
// Each State inheriting IState encapsulates the necessary components for a single action
public interface IState {

	// To be  called when an object enters the state
	void Enter (Actor actor);

	// To be called when an object exits the state
	void Exit ();

	// To be called in the Update, invokes a state action in the render step
	void StateUpdate ();

	// To be called in the FixedUpdate, invokes a state action in the physics step
	void StateFixedUpdate ();

	// Called in the Actor class when a Collider2D enters the trigger
	void OnTriggerEnter2D (Collider2D other);

	// Called in the Actor class when a Collider2D exits the trigger
	void OnTriggerExit2D (Collider2D other);
}
