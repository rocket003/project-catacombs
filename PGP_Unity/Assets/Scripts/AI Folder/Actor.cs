﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Collider2D)) ]

// Actor is a super class to be used for anything that moves or "acts" within the world
// Contains virtual property CurrentState and virtual methods Start, FixedUpdate
public abstract class Actor : MonoBehaviour {

	[SerializeField]
	protected IState defaultState = new EdgePatrolState ();

	[SerializeField]
	protected float speed = 1;
	[SerializeField]
	protected LayerMask groundMask;

	protected Rigidbody2D rBody;
	protected Collider2D co2D;

	protected virtual IState CurrentState { get; set; }

	public Vector2 Position { get { return rBody.position; } }
	public Collider2D Co2D { get { return co2D; } }
	public LayerMask GroundMask { get { return groundMask; } }

	public const float skinWidth = 0.01f;
	public const float rayLength = 0.1f + skinWidth;

	// Inititalization called before Start ()
	void Awake () {
		rBody = GetComponent<Rigidbody2D> ();
		co2D = GetComponent<Collider2D> ();
	}

	// Used for initialization
	protected virtual void Start () {
		CurrentState = defaultState;
		CurrentState.Enter (this);
	}

	// Called every frame
	protected virtual void Update () {
		CurrentState.StateUpdate ();
	}

	// Called every physics step, use for physics operations
	protected virtual void FixedUpdate () {
		CurrentState.StateFixedUpdate ();
	}

	protected virtual void OnTriggerEnter2D (Collider2D other) {
		CurrentState.OnTriggerEnter2D (other);
	}

	protected virtual void OnTriggerExit2D (Collider2D other) {
		CurrentState.OnTriggerExit2D (other);
	}

	// Determines whether or not the actor is connected to the ground
	// This is a pretty heavy method, so only call when needed
	protected bool Grounded () {
		Bounds bounds = co2D.bounds;
		bounds.Expand (skinWidth * -2);

		Vector2 rayOrigin = new Vector2 (bounds.center.x, bounds.min.y);

		RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.down, rayLength, groundMask);
		Debug.DrawRay (rayOrigin, Vector2.down * rayLength, Color.red);

		if (hit) {
			return true;
		}

		return false;
	}

	// Takes in a Vector2 defining the direction of movement, then normalizes the vector and moves based on direction and speed
	// Use exclusively in FixedUpdate
	public void Move (Vector2 dir) {
		rBody.MovePosition (rBody.position + dir.normalized * speed * Time.deltaTime);
	}
}
