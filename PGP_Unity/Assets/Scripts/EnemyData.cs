﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class EnemyData : MonoBehaviour {

	public int health;
	public int damage;
	private Blackboard bb;

	// Use this for initialization
	void Start () {
		bb = GetComponent<Blackboard> ();
	}

	public void takeDamage(int takeDamage){
		health = health - takeDamage;
		if (health <= 0) {
			bb.SendEvent ("Dead");
		} else {
			bb.SendEvent ("Hurt");
			GetComponent<Animator> ().SetTrigger ("Hurt");
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			bb.GetGameObjectVar ("Player").Value = other.gameObject;
			bb.SendEvent ("PlayerEnter");
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Player") {
			bb.SendEvent ("PlayerExit");
		}
	}
}
