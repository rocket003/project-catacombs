﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


	public GameObject spawnPoint;
	public GameObject savedPlayer; //The game object that respawns
	public GameObject savedCanvas; //The saved canvas
	public GameObject savedCamera; //The saved camera

	//Sounds
	public AudioClip sadDog;
	AudioSource aud;


	private GameObject currPlayer; //The current active player gameobject
	private GameObject currCanvas; //The active canvas
	private GameObject currCamera; //The active Camera


	// Use this for initialization
	void Start () {
		currPlayer = GameObject.FindGameObjectWithTag ("Player");
		currCanvas = GameObject.FindGameObjectWithTag ("HUD");
		currCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		aud = GameObject.Find ("GameManager").GetComponent<AudioSource> ();

		if (currPlayer == null) {
			Instantiate (savedPlayer, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currPlayer = GameObject.FindGameObjectWithTag ("Player");

		}
		if (currCanvas == null) {
			Instantiate (savedCanvas, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currCanvas = GameObject.FindGameObjectWithTag ("HUD");
		}
		if (currCamera == null) {
			Instantiate (savedCamera, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (currPlayer == null) {
			Instantiate (savedPlayer, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currPlayer = GameObject.FindGameObjectWithTag ("Player");

		}
		if (currCanvas == null) {
			Instantiate (savedCanvas, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currCanvas = GameObject.FindGameObjectWithTag ("HUD");
		}
		if (currCamera == null) {
			Instantiate (savedCamera, spawnPoint.transform.position, spawnPoint.transform.rotation);
			currCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		}
	}

	public void setSpawnPoint(GameObject newSpawnPoint){
		spawnPoint = newSpawnPoint;
	}

	public GameObject getSpawnPoint(){
		return spawnPoint;
	}
}
